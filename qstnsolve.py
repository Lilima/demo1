
#Q1 & Q2 find max and minimum no.
data = [2,4,5,6,10, 2,1,4]
data1 = [3,4,1,2,5,6]
max_all = max(data + data1)
min_all = min(data + data1)
print ("The maximum of both lists is : " + str(max_all))
print ("The minimum of both lists is : " + str(min_all))

#3.Find second Largest no. in a list

data = [2,4,5,6,10, 2,1,4]
data1 = [3,4,1,2,5,6]
mx = max(data[0], data[1])
secondmax = min(data[0], data[1])
n = len(data)
for i in range(2, n):
    if data[i] > mx:
        secondmax = mx
        mx = data[i]
    elif data[i] > secondmax and \
            mx != data[i]:
        secondmax = data[i]

print("Second highest number is : ", \
      str(secondmax))

#4.Find second minimum no. in a list
data = [2,4,5,6,10, 2,1,4]
mn = min(data[0], data[1])
secondmin = max(data[0], data[1])
n = len(data)
for i in range(2, n):
    if data[i] < mn:
        secondmin = mn
        mn = data[i]
    elif data[i] < secondmin and \
            mn != data[i]:
        secondmin = data[i]

print("Second minimum number is : ", \
      str(secondmin))
#5.sum of numbers  with step 1 from list (data) ; skip one index and add the next value
data = [2,4,5,6,10, 2,1,4]
data1 = [3,4,1,2,5,6]
data2 =(data + data1)
data2 = list(set(data2))
unwanted = [1, 3, 5]
for ele in sorted(unwanted, reverse = True):
    del data2[ele]

print(sum(data2))

#6.sum of numbers  with step 2 from list (data) ; skip one index and add the next value
data = [2,4,5,6,10, 2,1,4]
data1 = [3,4,1,2,5,6]
data2 =(data + data1)
data2 = list(set(data2))
unwanted = [1, 2,4, 5]
for ele in sorted(unwanted, reverse = True):
    del data2[ele]

print(sum(data2))

#Q7 Find duplicate in the list

data = [2,4,5,6,10, 2,1,4];
data1 = [3,4,1,2,5,6];

dupl = set() # create empty set

# loop trough the elements inside the list
for i in (data + data1):
    if (data + data1).count(i) > 1:
        dupl.add(i)
print(dupl)

#8.Find occurrence of each no.in the list
data = [2,4,5,6,10, 2,1,4];
data1 = [3,4,1,2,5,6];
out = {x: (data + data1).count(x) for x in set((data + data1))}
print ("Occurrence of all numbers in data and data1 is :\n "+ str(out))

#9.find the numbers which are present in data list but not in data1 list.
data = [2,4,5,6,10, 2,1,4];
data1 = [3,4,1,2,5,6];
presenteddata= list(set(data).difference(data1))
print("The number not present in datalist1 is:\n "+ str(presenteddata))

#or
data = [2,4,5,6,10, 2,1,4];
data1 = [3,4,1,2,5,6];

main_list = list(set(data) - set(data1))
print(main_list)

#10.write a function to swap two numbers
X = 10
Y = 20
temp = X
X = Y
Y = temp

print("Value of x:", X)
print("Value of y:", Y)

#or
a = 30
b = 20
print("\nBefore swap a = %d and b = %d" %(a, b))
a, b = b, a
print("\nAfter swaping a = %d and b = %d" %(a, b))

#11.Implement stack using list
#data [2,4,5,6,10, 2,1,4]
stack = []
len(stack) == 0
stack.append(2)
stack.append(4)
stack.append(5)
stack.append(6)
stack.append(10)
stack.append(2)
stack.append(1)
stack.append(4)
stack.append(3)
stack.append(4)
stack.append(1)
stack.append(2)
stack.append(5)
stack.append(6)
stack.pop()
stack.pop()
print(stack[-1])
#............Task for Tuessday(Related to Python Dictionary)...................

 #creates an empty List<Map>
 #1. find the highest total price cart
orderLines = [];
orderLines.append({ 'cart'  : 1, 'Item' :  [{'name': 'mobile', 'qty': 4, 'price': '2000'}, {'name': 'tab', 'qty': 2, 'price': '30000'}]});
orderLines.append({ 'cart'  : 2, 'Item' : [{'name': 'mobile', 'qty': 10, 'price': '2500'}, {'name': 'tab', 'qty': 3, 'price': '21000'}]});
orderLines.append({ 'cart'  : 3, 'Item' :  [{'name': 'mobile', 'qty': 12, 'price': '2300'}, {'name': 'tab', 'qty': 1, 'price': '30000'}]});
#d4 = orderLines(d1.items() + d2.items() + d3.items())
#print(d4)

print(orderLines)



























